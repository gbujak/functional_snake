#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include <SFML/Window/Keyboard.hpp>

#define CELL_COUNT 20
#define CELL_SIZE 20
#define GAP 5
#define WINDOW_SIZE CELL_SIZE * CELL_COUNT + GAP * (CELL_COUNT + 1)
#define FRAMERATE 15

typedef std::vector<sf::Vector2i> snake;

sf::Vector2i translate(sf::Vector2i v, int size = CELL_SIZE, int gap = GAP) {
    return {
        v.x * size + (v.x + 1) * gap,
        v.y * size + (v.y + 1) * gap
    };
}

snake moved_snake(const snake& s, sf::Vector2i speed, sf::Vector2i apple) {
    snake new_snake;
    new_snake.push_back(sf::Vector2i(
        (s[0].x + speed.x + CELL_COUNT) % CELL_COUNT,
        (s[0].y + speed.y + CELL_COUNT) % CELL_COUNT
    ));
    for (sf::Vector2i i : s) new_snake.push_back(i);
    if (apple != new_snake[0]) new_snake.pop_back();
    return new_snake;
}

sf::Vector2i gen_apple(const snake& s, int cell_count = CELL_COUNT) {
    sf::Vector2i apple;
    while (1) {
        apple.x = (float) (rand() % cell_count);
        apple.y = (float) (rand() % cell_count);
        for (sf::Vector2i i : s) if (i == apple) continue;
        break;
    };
    return apple;
}

sf::Vector2f vec2i_to_vec2f(sf::Vector2i v) {
    return sf::Vector2f((float) v.x, (float) v.y);
}

void draw_snake(sf::RenderWindow& win, const snake& s) {
    sf::RectangleShape rect;
    rect.setSize(sf::Vector2f(CELL_SIZE, CELL_SIZE));
    rect.setFillColor(sf::Color(0, 155, 0, 255));
    for (const sf::Vector2i& i : s) {
        rect.setPosition(vec2i_to_vec2f(translate(i)));
        win.draw(rect);
    }
}

bool snake_in_snake(const snake& s) {
    int size = s.size();
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            if (i == j) continue;
            if (s[i] == s[j]) return true;
        }
    }
    return false;
}

void draw_apple(sf::RenderWindow& win, const sf::Vector2i& apple) {
    sf::RectangleShape rect;
    rect.setSize(sf::Vector2f(CELL_SIZE, CELL_SIZE));
    rect.setFillColor(sf::Color(155, 0, 0, 255));
    rect.setPosition(vec2i_to_vec2f(translate(apple)));
    win.draw(rect);
}

sf::Vector2i get_movement_vector(const sf::Vector2i movement) {
    if (movement.x == 0) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            return { -1, 0 };
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            return { 1, 0 };
    } else if (movement.y == 0) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            return { 0, -1 };
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            return { 0, 1 };
    }
    return movement;
}

void game(sf::RenderWindow* win, sf::Event* ev,
        snake snk, sf::Vector2i movement,
        sf::Vector2i apple) {
    
    if (snake_in_snake(snk)) return;
    
    win->clear(sf::Color(0, 0, 0, 255));
    draw_snake(*win, snk);
    draw_apple(*win, apple);
    win->display();

    while (win->pollEvent(*ev))
        if (ev->type == sf::Event::Closed) return;

    game(
        win, ev,
        moved_snake(snk, movement, apple),
        get_movement_vector(movement),
        (snk[0] == apple) ? gen_apple(snk) : apple
    );
}

int main(void) {
    srand(time(nullptr));
    sf::RenderWindow window(
        sf::VideoMode(WINDOW_SIZE, WINDOW_SIZE),
        "Functional Snake"
    );
    sf::Event ev;
    window.clear();
    window.display();
    window.setFramerateLimit(FRAMERATE);
    
    const snake snk = {
        {9, 9}, {9, 8}, {9, 7}
    };
    const sf::Vector2i apple = { 9, 15 };
    game(&window, &ev, snk, {1, 0}, apple);

    window.close();
}